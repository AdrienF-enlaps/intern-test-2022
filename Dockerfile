# The Docker container build file

# base image :
FROM python:3.8
WORKDIR /classification_app
RUN pip install Pillow numpy
RUN pip3 install --extra-index-url https://google-coral.github.io/py-repo/ tflite_runtime
COPY . .


#ENTRY POINT
ENTRYPOINT ["python", "./app/main.py"]
